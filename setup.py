from setuptools import find_packages, setup

setup(
    name='irserving',
    version='1.0.0',
    license='BSD',
    description='IR webservice.',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'requests',
        'flask',
        'numpy',
        'pandas',
        'xgboost',
        'eventlet',
        'flask_socketio',
        'joblib'
    ],
    extras_require={
        'test': [
            'pytest',
        ],
    },
)