####################################################################################################
########## Service - Calls to the Zendesk API
####################################################################################################

import os
import json
from requests import get, post, put, delete
from os.path import exists, join, dirname, isfile
from os import remove
from urllib.parse import quote_plus

####################################################################################################
########## Utils
####################################################################################################
def format_salary(salary):
    if salary < 10000: return 0
    elif salary < 30000: return 1
    elif salary < 50000: return 2
    elif salary < 100000: return 3
    elif salary < 200000: return 4
    else: return 5

#Zendesk admin authentification
AUTH = ("aymeric.quesne@odigo.com", "Odigo@AIEZD")

#Dictionnaries linkink Zendesk IDs and feature names
zendeskIDtoKey = {
    360015752493: "customer_satisfaction",
    360019532193: "intent",
    360019532213: "customer_gender",
    360019532373: "customer_seniority",
    360019530134: "customer_age",
    360019532693: "customer_salary",
    360019689393: "agent_number",
    360021467154: "agent_gender",
    360021472833: "agent_age",
    360021472873: "agent_seniority",
    360021473913: "agent_satisfaction",
    360021473953: "call_duration",
    360021473993: "business_outcome",
}
keytoZendeskID = {
    "customer_satisfaction": 360015752493,
    "intent": 360019532193,
    "customer_gender": 360019532213,
    "customer_seniority": 360019532373,
    "customer_age": 360019530134,
    "customer_salary": 360019532693,
    "agent_number": 360019689393,
    "agent_gender": 360021467154,
    "agent_age": 360021472833,
    "agent_seniority": 360021472873,
    "agent_satisfaction": 360021473913,
    "call_duration": 360021473953,
    "business_outcome": 360021473993,
}

####################################################################################################
########## Tickets manipulation from front-end
####################################################################################################
def getAllInteractionsFromZendesk(onlySolvedTickets=True):
    '''
        DESCRIPTION: Gets all the interactions stored in the Zendesk instance thanks to the zendesk API (ticket.json)
        INPUT: Boolean = True to get only solved tickets from Zendesk, false to get them all
        OUTPUT: Array<interactions> = Zendesk's interactions
    '''
    interactions = []
    url = "https://d3v-odigo-lab.zendesk.com/api/v2/tickets.json"
    ticketLeft = True
    while ticketLeft:
        if url != None:
            print("Querying new interactions")
            data = get(url, auth=AUTH).json()
            if data.get("next_page") != None:
                url = data.get("next_page")
            else:
                ticketLeft = False
                url = None
        for ticket in data.get("tickets"):
            interaction = {}
            if not onlySolvedTickets or ticket.get("status") == "solved":
                for couple in ticket.get("custom_fields"):
                    interaction[zendeskIDtoKey[couple.get("id")]] = couple.get("value")
                interaction['id'] = ticket.get('id')
                interactions.append(interaction)
    print("Got " + str(len(interactions)) + " different tickets")
    if(onlySolvedTickets and len(interactions) > 0):
        with open(os.path.join(os.path.dirname(__file__), "..", "..", "models", "interactions.txt"), 'w+') as fd:
            json.dump({"interactions": interactions}, fd)
    else:
        if(exists(join(dirname(__file__), "..", "..", "models", 'interactions.txt'))):
            remove(join(dirname(__file__), "..", "..", "models", "interactions.txt"))
    return interactions

def createManyInteractions(interactions):
    '''
        DESCRIPTION: Create many interactions thanks to the create_many from the ticket.json API
        INPUT: Interactions to create: Array<interactions>
    '''
    print("Launching the creation of " + str(len(interactions)) + " interactions on Zendesk")
    base_url = ["https://d3v-odigo-lab.zendesk.com/api/v2/tickets/create_many.json"]
    interactionsToCreate = interactions[:]
    tickets = []
    agents_data = get_agents_data()
    while len(interactionsToCreate) > 0 or len(tickets) > 0:
        while len(tickets) < 99 and len(interactionsToCreate) > 0:
            interaction = interactionsToCreate.pop()
            custom_fields = [{"id": keytoZendeskID[attr], "value": value} for attr, value in interaction.items()]
            custom_fields.append({"id": 360021467154, "value": agents_data[interaction["agent_number"]]["agent_gender"]})
            custom_fields.append({"id": 360021472833, "value": agents_data[interaction["agent_number"]]["agent_age"]})
            custom_fields.append({"id": 360021472873, "value": agents_data[interaction["agent_number"]]["agent_seniority"]})
            tickets.append({
                "status": "solved",
                "type": "problem",
                "priority": "normal",
                "comment": {"body": "Intelligent Distribution Interaction"},
                "requester_id": 380040471834,
                "assignee_id": [375192318214, 375249447814, 375216812793, 375249536794, 375216867813][interaction.get("agent_number") - 1], #ID of the agent
                "group_id": 360003322614,  # ID of agent group corresponding to IR
                "custom_fields": custom_fields,
            })
        r = post("".join(base_url), json={"tickets": tickets}, auth=AUTH)
        print("sending " + str(len(tickets)) + " tickets, still " + str(len(interactionsToCreate)) + "to go")
        if str(r) == "<Response [200]>":
            tickets = []
    print("Done creating interactions")

def deleteAllTickets(interactions):
    '''
        DESCRIPTION: This function deletes all the interactions it is given from the Zendesk instance. It uses the destroy_many route
                    from the zendesk API (ticket.json) to bulk delete tickets. It return the count of interactions deleted
        INPUT: Array<Interactions> = interactions to delete
        OUTPUT: Number = count of interaction deleted
    '''
    interactionsToDelete = interactions[:]
    tickets = []
    count = 0
    while len(interactionsToDelete) > 0 or len(tickets) > 0:
        ########## Group tickets by 100 or less (requirement of the destroy_many route)
        while len(tickets) < 99 and len(interactionsToDelete) > 0:
            interaction = interactionsToDelete.pop()
            tickets.append(interaction["id"])
        ########## Sending the destroy request
        base_url = ["https://d3v-odigo-lab.zendesk.com/api/v2/tickets/destroy_many.json?ids="]
        for ID in tickets:
            base_url[0] += str(ID) + ","
        base_url[0] = base_url[0][:-1] # <--- Removes last coma
        r = delete("".join(base_url), auth=AUTH)
        print("Deleted " + str(len(tickets)) + " tickets, still " + str(len(interactionsToDelete)) + "to go")
        if str(r) == "<Response [200]>":
            tickets = []
    return count

def create_ZD_interaction(agent, customer):
    '''
    DESCRIPTION: Creates a Zendesk interaction including the information we have at the beggining of a call
    INPUT: Object, Object = Agent data, Customer data
    OUTPUT: string = interaction ID
    '''

    base_url = ["https://d3v-odigo-lab.zendesk.com/api/v2/tickets.json"]
    request_body = {
        "ticket": {
            "status": "pending",
            "type": "problem",
            "priority": "normal",
            "comment": {"body": "Intelligent Distribution Interaction"},
            "requester_id": customer.get("id"),
            "assignee_id": agent.get("id"),
            "group_id": 360003322614,  # ID of agent group corresponding to IR
            "custom_fields": [
                {"id": 360019532193, "value": customer.get("intent")},
                {"id": 360019532213, "value": customer.get("customer_gender")},
                {"id": 360019532373, "value": customer.get("customer_seniority")},
                {"id": 360019530134, "value": customer.get("customer_age")},
                {"id": 360019532693, "value": customer.get("customer_salary")},
                {"id": 360019689393, "value": agent.get('agent_number')},
                {"id": 360021467154, "value": agent.get('agent_gender')},
                {"id": 360021472833, "value": agent.get('agent_age')},
                {"id": 360021472873, "value": agent.get('agent_seniority')},
            ],
        }
    }
    ticketData = post("".join(base_url), json=request_body, auth=AUTH).json().get("ticket")
    print(ticketData)
    return ticketData.get("id", -1)

def insert_satisfaction(interaction_ID, satisfaction):
    '''
    DESCRIPTION: Inserts customer satisfaction (1 to 5) into an already existing Zendesk interaction (this function is used by the IVR)
    INPUT: string, number = Interaction ID, Satisfaction
    '''
    base_url = ["https://d3v-odigo-lab.zendesk.com/api/v2/tickets/", str(interaction_ID), ".json"]
    request_body = {"ticket": {"custom_fields": [{"id": 360015752493, "value": satisfaction}]}}
    r = put("".join(base_url), json=request_body, auth=AUTH)
####################################################################################################
########## User manipulation
####################################################################################################
def get_customer_data(calling_nb):
    '''
        DESCRIPTION: Retrieve customer data from it's calling number. This function is used to retrieve information needed for 
                     routing when a customer directly calls the IVR.
        INPUT: Phone number calling the IVR: string
        OUTPUT: Customer data: Object
    '''
    
    base_url = ["https://d3v-odigo-lab.zendesk.com/api/v2/search.json?query=phone%3A", quote_plus(calling_nb)]
    data = get("".join(base_url), auth=AUTH).json().get("results")[0]

    print('Retrieved user data for the ' + str(calling_nb) + ' Phone number')
    print(data)

    if data is not None:
        return {
            "id": data.get('id'),
            "customer_gender": data.get('user_fields')['gender'],
            "customer_seniority": data.get('user_fields')['client_since'],
            "customer_age": data.get('user_fields')['age'],
            "customer_salary": format_salary(data.get('user_fields')['salary'])
        }
    else:
        return {"message": "User doesn't exist in Zendesk database"}

def get_agents_data():
    '''
    DESCRIPTION: Retrieves agent data. The response if a dictionnary which keys are agents_number (1 to 5). Each value associated
                 to thoses keys is a dicionnary containing all the agent data
    OUTPUT: Object = Agent data
    '''

    base_url = ["https://d3v-odigo-lab.zendesk.com/api/v2/search.json?query=", quote_plus("group:IR_agents type:user")]

    r = get("".join(base_url), auth=AUTH)
    data = r.json()

    agents_data = {}
    for agent in data.get("results"):
        agents_data[int(agent.get("name")[-2])] = {
            "id": agent.get("id"),
            "agent_number": int(agent.get("name")[-2]),
            "agent_name": agent.get("name"),
            "agent_email": agent.get("email"),
            "agent_zendeskID": agent.get("id"),
            "agent_age": agent.get("user_fields").get("age"),
            "agent_gender": agent.get("user_fields").get("gender"),
            "agent_seniority": agent.get("user_fields").get("client_since"),
            "agent_skill": agent.get("user_fields").get("agent_skill"),
        }
    return agents_data
