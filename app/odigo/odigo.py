####################################################################################################
########## Service - Calls to the odigo API
####################################################################################################
from requests import get

def query_agents_statuses():
    '''
        DESCRIPTION: returns agents availibility from the odigo API (12 means available, others like 0 or 13 mean not available)
        OUTPUT: Agent Availability: Object     (dictionnary which keys are agent_numbers and values are availibility of the agent)
    '''
    
    email_to_agent_number = {
        'agent167ddi@demo.com': 1,
        'agent167_1@demo.com': 2,
        'agent167_2@demo.com': 3,
        'agent167_3@demo.com': 4,
        'agent167_4@demo.com': 5
    }
    statuses = {}
    API_URL = ["https://routing-de01.prosodie.com/de01/servlet/SupervisionSnapshot?lg=WSOTH&pw=Prosodie1&ft=5&tb=4"]
    
    for agent in ['agent167ddi@demo.com', 'agent167_1@demo.com', 'agent167_2@demo.com', 'agent167_3@demo.com', 'agent167_4@demo.com']:
        API_URL.append("&ob=" + agent) 
        
    data = get("".join(API_URL)).json()
    
    if data.get("message") == "Maximum number of requests allowed reached.":
        return {"error": "too many requests"}
    else:
        for agent in data.get("objects"):
            status = [x.get("value") for x in agent.get("statistics") if x.get("id") == 7]
            statuses[email_to_agent_number[agent.get("key")]] = int(status[0])
        return statuses