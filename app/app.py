####################################################################################################
########## App
####################################################################################################

from flask import Flask, request, make_response, abort, send_from_directory
from flask.json import jsonify
from os import getcwd, remove
from os.path import exists, join, dirname
from requests import get
from json import dump, dumps, load
from flask_socketio import SocketIO

from predictor import Predictor
from zendesk import deleteAllTickets, getAllInteractionsFromZendesk, createManyInteractions, get_customer_data, get_agents_data, create_ZD_interaction,  insert_satisfaction
from odigo import query_agents_statuses

def create_app(test_conf=None):

####################################################################################################
########## Initialize app
####################################################################################################
    app = Flask(__name__, static_folder="static")
    app.config["DEBUG"] = True
    socketio = SocketIO(app)

    with app.app_context():
        #Getting interactions from Zendesk
        interactions = getAllInteractionsFromZendesk(onlySolvedTickets=True)
        if(len(interactions) == 0):
            print("No interactions stored in Zendesk")
        
        #Adding the data and formating the four models, followed by automatic training
        else:
            CustomerSatisfactionModel = Predictor(
                target="customer_satisfaction",
                categorical_features={"customer_gender": [0, 1, 2], "agent_gender": [0, 1, 2], "agent_number": [1, 2, 3, 4, 5], "customer_salary": [0, 1, 2, 3, 4, 5], "intent": [0, 1, 2, 3, 4, 5]},
                features=["customer_gender", "customer_age", "customer_salary", "intent", "customer_seniority", "agent_number", "agent_gender", "agent_age", "agent_seniority", "customer_satisfaction"],
                interactions=interactions
            )
            AgentSatisfactionModel = Predictor(
                target="agent_satisfaction",
                categorical_features={"customer_gender": [0, 1, 2], "agent_gender": [0, 1, 2], "agent_number": [1, 2, 3, 4, 5], "customer_salary": [0, 1, 2, 3, 4, 5], "intent": [0, 1, 2, 3, 4, 5]},
                features=["customer_gender", "customer_age", "customer_salary", "intent", "customer_seniority", "agent_number", "agent_gender", "agent_age", "agent_seniority", "agent_satisfaction"],
                interactions=interactions
            )
            CallDurationModel = Predictor(
                target="call_duration",
                categorical_features={"customer_gender": [0, 1, 2], "agent_gender": [0, 1, 2], "agent_number": [1, 2, 3, 4, 5], "customer_salary": [0, 1, 2, 3, 4, 5], "intent": [0, 1, 2, 3, 4, 5]},
                features=["customer_gender", "customer_age", "customer_salary", "intent", "customer_seniority", "agent_number", "agent_gender", "agent_age", "agent_seniority", "call_duration"],
                interactions=interactions
            )
            BusinessOutcomeModel = Predictor(
                target="business_outcome",
                categorical_features={"customer_gender": [0, 1, 2], "agent_gender": [0, 1, 2], "agent_number": [1, 2, 3, 4, 5], "customer_salary": [0, 1, 2, 3, 4, 5], "intent": [0, 1, 2, 3, 4, 5]},
                features=["customer_gender", "customer_age", "customer_salary", "intent", "customer_seniority", "agent_number", "agent_gender", "agent_age", "agent_seniority", "business_outcome"],
                interactions=interactions
            )

####################################################################################################
########## Routes - Serve the front end files
####################################################################################################
    @app.route("/")
    def serve_html():
        return send_from_directory(join(getcwd(), "static"), "index.html")

    @app.route("/<path:path>")
    def serve_js(path):
        return send_from_directory(join(getcwd(), "static"), path)

####################################################################################################
########## Routes - Called directly by the demo's IVR
####################################################################################################
    @app.route("/predict", methods=["GET"])
    def predict():
        '''
        DESCRIPTION: Route called by the IVR to know which agent it should call
        INPUT: Calling number: string
        OUTPUT: Skill to call: string (Odigo skill, each agent has its one)
                InteractionID: string (ID of the interaction from Zendesk)
        '''
        if request.args.get("calling") is not None:\
            #Querying agent and customer data (customer data is queried thanks to the phone number calling the IVR)
            agentData = get_agents_data()
            if request.args.get("calling")[0] == "0":
                formatted_nb = "+33" + request.args.get("calling")[1:]
            else:
                formatted_nb = request.args.get("calling")[:]
            customerData = get_customer_data(formatted_nb)
            customerData['intent'] = request.args.get("intent")
            
            #Getting predictions from trained algorithms for each target
            prediction = {}
            for target in ['customer_satisfaction', "agent_satisfaction", "call_duration", "business_outcome"]:
                model = Predictor(target=target)
                prediction[target] = model.predict_best_agents(customerData, agentData)

            #Calculating the score of each agents thanks to the predictions and the target aim
            scores = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}
            with open(join(dirname(__file__), "..", "models", "targets.txt")) as fd:
                targets = load(fd)
                for target in ['customer_satisfaction', "agent_satisfaction", "call_duration", "business_outcome"]:
                    for pred in prediction[target]:
                        scores[pred['agent_number']] += pred['proba']*targets[target]
            
            #Finding the best connected agent
            agentAvailability = query_agents_statuses()
            bestScore = 0
            bestAgent = 0
            for i in range(1, 6):
                if scores[i] > bestScore and agentAvailability[i] == 12:
                    bestScore = scores[i]
                    bestAgent = i
            print("Best agent: " + str(bestAgent))
            
            #Creating the Zendesk interaction related to the interaction
            interactionID = create_ZD_interaction(agentData[bestAgent], customerData)
            if bestAgent > 0:
                socketio.emit("new_call", {"customer": customerData})
                return dumps({'skill': agentData[bestAgent]['agent_skill'], "interaction_id": interactionID})
            else:
                return abort(400)
        else:
            return abort(400)

    @app.route("/add_rating", methods=["GET"])
    def add_rating():
        '''
        DESCRIPTION: Route called by the IVR to insert customer satisfaction into the Zendesk interaction
        INPUT: InteractionID: string
               Satisfaction: number
        '''
        if request.args.get("interaction_id") is None or request.args.get("satisfaction") is None:
            print("Your request did not provide the required arguments for this request")
            return abort(400, "Your request did not provide the required arguments for this request.")
        else:
            insert_satisfaction(request.args.get("interaction_id"), request.args.get("satisfaction"))
            socketio.emit("rating_added", {"interaction_ID": request.args.get("interaction_id"), "rating": request.args.get("satisfaction")})
        return jsonify({"status": "OK"})

####################################################################################################
########## Routes - Called by the front-end
####################################################################################################
    @app.route("/getInteractions", methods=["GET"])
    def get_interactions():
        interactions = []
        if exists(join(dirname(__file__), "..", "models", "interactions.txt")):
            with open(join(dirname(__file__), "..", "models", "interactions.txt")) as fd:
                interactions = load(fd).get('interactions')
                response = make_response(jsonify({"interactions": interactions}))
                response.headers["Access-Control-Allow-Origin"] = "*"
                return response
        else:
            response = make_response(jsonify({'message': 'No interactions to serve'}))
            response.headers["Access-Control-Allow-Origin"] = "*"
            return response

    @app.route("/agentInfo", methods=["GET"])
    def get_agent_info():
        agent_info = get_agents_data()
        response = make_response(jsonify({"agent_info": agent_info}))
        response.headers["Access-Control-Allow-Origin"] = "*"
        return response
    
    @app.route("/zendeskCount", methods=["GET"])
    def sendZendeskCount():
        count = get("https://d3v-odigo-lab.zendesk.com/api/v2/tickets.json", auth=("aymeric.quesne@odigo.com", "Odigo@AIEZD")).json().get("count")
        print(str(count) + ' interactions created by Zendesk')
        response = make_response(jsonify({"count": count}))
        response.headers["Access-Control-Allow-Origin"] = "*"
        return response
    
####################################################################################################
########## SocketIO - Connections
####################################################################################################

    #WORK AROUND
    #The flask application can process only one in request at the time and training algorithm is preventing the back-end to answer
    #other requests. When the training is too long, sockets disconnect because they can't pass the handshake. When they reconnect,
    #if it is detected that not all models are trained, it launches the new training.
    @socketio.on("connect")
    def handle_message():
        areAlgoTrained = []
        for target in ['customer_satisfaction', "agent_satisfaction", "call_duration", "business_outcome"]:
            areAlgoTrained.append(exists(join(dirname(__file__), "..", "models", target + '_trained_model.pkl')))
        print('Algorithm training: ' + str(areAlgoTrained))
        print('does the interaction file exist: ' + str(exists(join(dirname(__file__), "..", "models", 'interactions.txt'))))
        if(exists(join(dirname(__file__), "..", "models", 'interactions.txt'))):
            targets = ['customer_satisfaction', "agent_satisfaction", "call_duration", "business_outcome"]
            for i in [0, 1, 2, 3]:
                if not (exists(join(dirname(__file__), "..", "models", targets[i] + '_trained_model.pkl'))):
                    print('Launching next training')
                    if(i == 0):
                        socketio.emit("refreshOver")
                    else:
                        socketio.emit("trainingOver", {"target": targets[i - 1]})
                        return
        else:
            socketio.emit("noInteractions")
                
    @socketio.on("disconnect")
    def disconnect():
        print("sockets disconnecting!")

####################################################################################################
########## SocketIO - Model handling
####################################################################################################
    @socketio.on("train")
    def train(data):
        target = data.get('target')
        print("Beggining the training of the " + target + " model")
        with open(join(dirname(__file__), "..", "models", "interactions.txt")) as fd:
            interactions = load(fd).get('interactions')
        model = Predictor(
            target=target,
            categorical_features={"customer_gender": [0, 1, 2], "agent_gender": [0, 1, 2], "agent_number": [1, 2, 3, 4, 5], "customer_salary": [0, 1, 2, 3, 4, 5], "intent": [0, 1, 2, 3, 4, 5]},
            features=["customer_gender", "customer_age", "customer_salary", "intent", "customer_seniority", "agent_number", "agent_gender", "agent_age", "agent_seniority", target],
            interactions=interactions,
            needTraining=True
        )
        print("done training the " + target + " model")
        socketio.emit("trainingOver", {"target": target})

####################################################################################################
########## SocketIO - Zendesk interactions handling
####################################################################################################
    @socketio.on("refreshInteractions")
    def refreshInteractions():
        print("Refreshing interactions from Zendesk")
        for target in ['customer_satisfaction', "agent_satisfaction", "call_duration", "business_outcome"]:
            if exists(join(dirname(__file__), "..", "models", target + "_trained_model.pkl")):
                remove(join(dirname(__file__), "..", "models", target + "_trained_model.pkl"))
        interactions = {}
        interactions['interactions'] = getAllInteractionsFromZendesk(onlySolvedTickets=True)
        with open(join(dirname(__file__), "..", "models", "interactions.txt"), 'w+') as fd:
            dump(interactions, fd)
        print("Done refreshin interactions, got " + str(len(interactions)) + " solved interactions")
        socketio.emit("refreshOver")

    @socketio.on("uploadInteractionsToZendesk")
    def uploadInteractionsToZendesk(data):
        for target in ['customer_satisfaction', "agent_satisfaction", "call_duration", "business_outcome"]:
            if exists(join(dirname(__file__), "..", "models", target + "_trained_model.pkl")):
                remove(join(dirname(__file__), "..", "models", target + "_trained_model.pkl"))
        interactions = getAllInteractionsFromZendesk(False)
        print(str(len(interactions)) + " interactions to delete")
        deleteAllTickets(interactions)
        interactionNumber = len(data.get("interactions"))
        print("SOCKETIO:", "Uploading " + str(interactionNumber) + " interactions to Zendesk")
        with open(join(dirname(__file__), "..", "models", "interactions.txt"), 'w+') as fd:
            dump(data, fd)
        createManyInteractions(data.get("interactions"))
        socketio.emit("onUploadOver", {"notification": "Added your interactions to the Zendesk instance"})
    
    @socketio.on("predict_best_agent")
    def predict_best_agent(data):
        '''
            DESCRIPTION: Returns the score (via socket) of each agents for each target (the 4 ML models)
            INPUT: Customer data: Object     (all the features except agent features and the target)
        '''
        agentData = get_agents_data()
        prediction = {}
        for target in ['customer_satisfaction', "agent_satisfaction", "call_duration", "business_outcome"]:
            model = Predictor(target=target)
            prediction[target] = model.predict_best_agents(data.get("data"), agentData)
        socketio.emit("predict_best_agent_result", {"prediction": prediction})

####################################################################################################
########## SocketIO - Data handling
####################################################################################################
    @socketio.on("targetChange")
    def writeTargetChange(targets):
        print('Getting new targets')
        with open(join(dirname(__file__), "..", "models", "targets.txt"), 'w+') as fd:
            fd.write(str(dumps(targets)))

    @socketio.on("agentStatus")
    def sendAgentsStatuses(data):
        '''
        Sends the odigo availibility of each agents to the front end via socket
        OUTPUT: Object <agent_number|availability>
        '''
        agentStatus = query_agents_statuses()
        if agentStatus != {"error": "too many requests"}:
            socketio.emit("agentStatusResponse", {"agentsStatuses": agentStatus})

####################################################################################################
########## Error handling
####################################################################################################
    @app.errorhandler(404)
    def not_found(error):
        return make_response(jsonify({"error": "Not found"}), 404)

    @app.errorhandler(400)
    def internal_error(error):
        return make_response(jsonify({"error": "Bad request", "description": error.description}), 400)

    return (app, socketio)