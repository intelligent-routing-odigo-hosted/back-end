####################################################################################################
########## Service - Predictor class (leverages ML algorithms)
####################################################################################################
import os
import numpy as np
import pandas as pd
import xgboost as xgb
import joblib
from pathlib import Path
import random
import json
from time import gmtime, strftime


class Predictor:
    def __init__(self, target, categorical_features=None, features=None, interactions=None, needTraining=False):
        '''
            DESCRIPTION: constructor of the Predictor class
            INPUT: target: string     (what the algorithm is supposed to guess)
                   categorical_features: List<string>     (list of all the categorical features the model will work on)
                   features: List<string>     (list of all the features, including categorical, numerical and the target)
                   interactions: List<Object>    (data the algorithm will work on)
                   needTraining: boolean     (True to train the algorithm after __init__)
        '''
        
        self.target = target
        self.dataFilepath = Path("./models/") / (self.target + "_text_model.txt")
        self.modelFilepath = Path("./models/") / (self.target + '_trained_model.pkl')
        self.needTraining = needTraining
        
        #Loading trained model
        if os.path.isfile(os.path.join(os.path.dirname(__file__), "..", "..", "models", self.target + "_trained_model.pkl")):
            print('Loading trained model')
            self.bst = joblib.load(self.modelFilepath)
            self.schema = self.bst.feature_names
        
        #Initiating data file
        if(categorical_features is not None and features is not None):
            print('Creating model ' + self.target + "_text_model.txt and loading " + str(len(interactions)) + ' interactions' )
            create_base_int_file(self.dataFilepath, categorical_features, features, target)

        #Loading interactions in the data file
        if(interactions is not None):
            print('Adding ' + str(len(interactions)) + ' interaction to the ' + self.target + ' model')
            with open(self.dataFilepath) as fd:
                prev_lines = fd.readlines()
                for interaction in interactions:
                    row = '\n'
                    for col in features:
                        row += str(interaction[col])+','
                    row = row[:-1] #removes trailing comma
                    prev_lines.append(row)
                with open(self.dataFilepath, 'w') as fd2:
                    fd2.writelines(prev_lines)
                    fd2.flush()
            self.needTraining = True

        #Loading features written in the model and loading interactions data
        with open(self.dataFilepath) as f:
            meta = json.loads(f.readline().rstrip('\r\n'))
            self.categorical_features = meta.get('categorical_features')
            self.numerical_features = meta.get('numerical_features')
            self.columns = f.readline().rstrip('\r\n').split(',')
            lines = f.readlines()
            interactions_to_train = np.zeros((len(lines), len(self.columns)))
            for i, line in enumerate(lines):
                interactions_to_train[i, :] = [int(nb) for nb in line.rstrip('\r\n').split(',')]
            self.inter_df = pd.DataFrame(interactions_to_train, columns=self.columns)
        
        #Training the model and saving it
        if self.needTraining:
            print("Beginning the training of the " + self.target + " model")
            self.train()

    def train(self):
        '''
            DESCRIPTION: trains the model, and then saves it
        '''
        schema = build_dummies_columns(self.categorical_features, self.numerical_features)
        self.train_df = self.inter_df.apply(lambda inter: series_fill_w_zeros(series_get_dummies(inter, self.categorical_features), schema),axis=1)
        bst, schema = train_xgb_full_int(self.train_df, self.target)
        self.bst = bst
        self.schema = bst.feature_names
        joblib.dump(self.bst, self.modelFilepath)

    def predict_best_agents(self, data, agentData):
        '''
            DESCRIPTION: Calculates the score of each agent (injecting their personnal information)
            INPUT: data: Object     (must contain every feature the algorithm has been initialized with except the target and agent features)
                   agentData: Object     (agent features, with the form of get_agents_data())
            OUTPUT: list of predictions: List<Object>
        '''
        agents = self.categorical_features.get('agent_number')
        preds = []
        data.pop('id', None)
        for agent_number in agents:
            input_data = pd.Series({
                **data,
                "agent_number": agent_number,
                "agent_age": agentData[agent_number]['agent_age'],
                "agent_gender": agentData[agent_number]['agent_gender'],
                "agent_seniority": agentData[agent_number]['agent_seniority']
            })
            input_data_w_dummies = series_get_dummies(
                input_data,
                self.categorical_features
            )
            a = series_fill_w_zeros(input_data_w_dummies, self.schema)
            b = a.reindex(sorted(a.keys()), axis=1)
            xgb_data = xgb.DMatrix(
                np.array(b).reshape((1, len(b))),
                feature_names=b.keys()
            )
            pred = self.bst.predict(xgb_data)
            preds.append({'agent_number': agent_number, 'proba': float(pred[0])})
        return preds

def train_xgb_full_int(inters, target, xgb_params={}, num_rounds=20):
    '''
        DESCRIPTION: Labels interactions into binary classes for classification, and then trains the algorithm
    '''
    if target == 'customer_satisfaction' or target == 'agent_satisfaction':
        inters['label'] = (inters[target] >= 4).map(lambda x: 1 if x else 0)
    elif target == 'call_duration':
        inters['label'] = (inters[target] < 10).map(lambda x: 1 if x else 0)
    elif target == 'business_outcome':
        inters['label'] = (inters[target] > 0).map(lambda x: 1 if x else 0)
    else:
        print('No binary rule for this target')
    labels_df = inters['label'].values
    train_data = inters.drop(['label', target], axis=1)

    train_data = train_data.reindex(sorted(train_data.columns), axis=1)
    data = xgb.DMatrix(train_data.values, labels_df, feature_names=train_data.keys())
    
    booster_params = {'max_depth': 6, 'eta': 1.75, 'silent': 1, 'objective': 'binary:logistic'}
    booster_params.update(xgb_params)
    bst = xgb.train(booster_params, data, num_rounds)
    return bst, train_data


def build_dummies_columns(categorical_features, numerical_features):
    '''
        DESCRIPTION: Creates dummies from categorical features and join them to numerical features
    '''
    columns = []
    for key in categorical_features.keys():
        for val in categorical_features[key]:
            columns.append(key+'_'+str(val))
    columns += numerical_features
    return columns


def series_fill_w_zeros(series, schema):
    for column in schema:
        if column not in series:
            series = series.append(pd.Series({column: 0}))
    return series


def series_get_dummies(series, categ_poss):
    for col in series.keys():
        if col in categ_poss:
            for poss in categ_poss.get(col):
                series = series.append(pd.Series({col+'_'+str(poss): 0}))
            series[col+'_'+str(int(series[col]))] = 1
            series = series.drop(col)
    return series

def create_base_int_file(filename, categorical_features, features, target):
    '''
        DESCRIPTION: Writes the two first lines of the model's data file 
        INPUT: EXPLICIT
    '''
    model_descriptor = {"target": target}
    model_descriptor.update({"categorical_features": categorical_features})
    numerical_features = []
    
    for feature in features:
        if feature not in list(categorical_features.keys()):
            numerical_features.append(feature)
    model_descriptor.update({"numerical_features": numerical_features})
    with open(filename, 'w+') as fd:
        fd.write(json.dumps(model_descriptor) + '\n')
        fd.write(features[0])
        for i in range(1, len(features)):
            fd.write(','+str(features[i]))