####################################################################################################
########## Initiates the app
####################################################################################################

from app import create_app
from flask_socketio import SocketIO

if __name__ == "__main__":
    (app, socketio) = create_app()
    socketio.run(app, host="0.0.0.0", port="3615")